<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('mroleparents')->insert([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'rolename' => 'Admin',
            'created_at' => now(),
            'created_by' => 'Seeder',
            'updated_at' => now(),
            'updated_by' => 'Seeder',
        ]);
    }
}
