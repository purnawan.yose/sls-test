<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $topRole = DB::table('mroleparents')->first();
        DB::table('users')->insert([
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'), // You can use the Hash facade to securely hash the password.
            'role' => $topRole->id,
            'remember_token' => Str::random(10), // You can generate a random token for the remember_token.
            'created_by' => 'seeder',
            'updated_by' => 'seeder',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
