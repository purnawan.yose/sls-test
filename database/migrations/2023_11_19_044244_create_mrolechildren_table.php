<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mrolechildren', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('roleid');
            $table
              ->foreign('roleid')
              ->references('id')
              ->on('mroleparents');
            $table->uuid('menuid');
            $table
              ->foreign('menuid')
              ->references('id')
              ->on('mmenus');
            $table->uuid('functionid');
            $table
              ->foreign('functionid')
              ->references('id')
              ->on('mfunctions');
            $table->timestamps();
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mrolechildren');
    }
};
