<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mitemuoms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('internaliditem');
            $table->foreign('internaliditem')->references('id')->on('mitems');
            $table->uuid('internaliduom');
            $table->foreign('internaliduom')->references('id')->on('muoms');
            $table->integer('isdefault');
            $table->decimal('konversi', 25,6);
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mitemuoms');
    }
};
