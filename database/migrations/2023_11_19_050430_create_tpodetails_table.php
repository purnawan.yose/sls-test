<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tpodetails', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('internalidpo');
            $table->foreign('internalidpo')->references('id')->on('tpoheaders');
            $table->uuid('internaliditem');
            $table->foreign('internaliditem')->references('id')->on('mitems');
            $table->integer('nourut')->nullable();
            $table->decimal('qty', 25, 6)->nullable();
            $table->string('uom', 45)->nullable()->default('NULL');
            $table->decimal('qtypack', 25, 6)->nullable();
            $table->string('uompack', 45)->nullable()->default('NULL');
            $table->decimal('price', 25, 6)->nullable();
            $table->decimal('discpersen1', 5, 2)->nullable();
            $table->decimal('discvalue1', 25, 6)->nullable();
            $table->decimal('discpersen2', 5, 2)->nullable();
            $table->decimal('discvalue2', 25, 6)->nullable();
            $table->decimal('discpersen3', 5, 2)->nullable();
            $table->decimal('discvalue3', 25, 6)->nullable();
            $table->decimal('subtotal', 25, 6)->nullable();
            $table->string('remarks', 250)->nullable()->default('NULL');
            $table->timestamps();
            $table->string('created_by')->nullable()->default('NULL');
            $table->string('updated_by')->nullable()->default('NULL');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tpodetails');
    }
};
