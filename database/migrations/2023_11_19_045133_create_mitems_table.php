<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mitems', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('internalidcategory');
            $table
              ->foreign('internalidcategory')
              ->references('id')
              ->on('mcategories');
            $table->string('barcodeid', 50)->unique();
            $table->string('itemid', 50)->unique();
            $table->string('itemname', 250);
            $table->string('uomdefault', 50);
            $table->string('uompackdefault', 50);
            $table->string('remarks', 250);
            $table->double('pricebeli', 25, 6);
            $table->integer('isactive');
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mitems');
    }
};
