<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mcompanies', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('companyid', 50)->unique();
            $table->string('companyname', 250);
            $table->text('address');
            $table->string('phone', 250);
            $table->string('email', 250);
            $table->string('npwp', 250);
            $table->tinyInteger('isactive');
            $table->timestamps();
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mcompanies');
    }
};
