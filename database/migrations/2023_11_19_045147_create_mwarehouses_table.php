<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mwarehouses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('iternalidcompany');
            $table->foreign('iternalidcompany')->references('id')->on('mcompanies');
            $table->string('warehouseid', 50)->unique();
            $table->string('warehousename', 250);
            $table->string('address');
            $table->integer('isactive');
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mwarehouses');
    }
};
