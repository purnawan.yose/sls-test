<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mmenufunctions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('menuid');
            $table->uuid('functionid');
            $table->timestamps();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table
              ->foreign('menuid')
              ->references('id')
              ->on('mmenus');
            $table
                ->foreign('functionid')
                ->references('id')
                ->on('mfunctions');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mmenufunctions');
    }
};
