<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mmenus', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('menuname', 250);
            $table->string('menuroute', 45);
            $table->string('remark', 250)->nullable();
            $table->string('parentid', 50)->nullable();
            $table->string('menuicon', 100)->nullable();
            $table->timestamps();
            $table->string('created_by', 255)->nullable();
            $table->string('updated_by', 255)->nullable();
            $table->tinyInteger('disabled');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mmenus');
    }
};
