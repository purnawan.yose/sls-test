<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tpoheaders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('internalidcompany');
            $table->foreign('internalidcompany')->references('id')->on('mcompanies');
            $table->uuid('internalidwarehouse');
            $table->foreign('internalidwarehouse')->references('id')->on('mwarehouses');
            $table->uuid('internalidsupplier');
            $table->foreign('internalidsupplier')->references('id')->on('msuppliers');
            $table->string('nopo', 45)->unique();
            $table->date('tanggalpo')->nullable();
            $table->date('tanggalestimasitiba')->nullable();
            $table->integer('toppo')->nullable();
            $table->integer('incpp')->nullable();
            $table->string('dibuatoleh', 150)->nullable()->default('NULL');
            $table->string('remarks', 250)->nullable()->default('NULL');
            $table->integer('void')->nullable()->default(0);
            $table->decimal('subtotal', 25, 6)->nullable();
            $table->decimal('discpersenheader', 5, 2)->nullable();
            $table->decimal('discvalueheader', 25, 6)->nullable();
            $table->decimal('discvaluedetail', 25, 6)->nullable();
            $table->decimal('disctotal', 25, 6)->nullable();
            $table->decimal('others', 25, 6)->nullable();
            $table->decimal('ongkoskirim', 25, 6)->nullable();
            $table->decimal('grandtotal', 25, 6)->nullable();
            $table->timestamps();
            $table->string('created_by')->nullable()->default('NULL');
            $table->string('updated_by')->nullable()->default('NULL');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tpoheaders');
    }
};
