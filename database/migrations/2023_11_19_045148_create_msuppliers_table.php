<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('msuppliers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('supplierid')->unique();
            $table->string('suppliername');
            $table->string('alamat');
            $table->string('phone');
            $table->string('email');
            $table->string('remarks');
            $table->integer('isactive');
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('msuppliers');
    }
};
