<?php

namespace App\Http\Controllers;

use App\Models\Mfunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class MfunctionController extends Controller
{

  public function create(Request $request)
  {
    try {

      $data = $request->data;

      $check = Mfunction::where('id', $data['id'])->first();
      if ($check) {
        $response = [
          'status' => Response::HTTP_FOUND,
          'message' => 'The given data was invalid.',
          'data' => null,
        ];
        return response()->json($response, Response::HTTP_FOUND);
      } else {
        DB::beginTransaction();

        $data = Mfunction::create($data);

        DB::commit();
        $response = [
          'status' => Response::HTTP_OK,
          'message' => 'successfully added.',
          'data' => $data,
        ];
        return response()->json($response, Response::HTTP_OK);
      }
    } catch (\Exception $e) {
      DB::rollback();
      $response = [
        'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
        'message' => 'The given data was invalid.' . $e->getMessage(),
        'data' => null,
      ];
      return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  public function update(Request $request){
    try {

        $data = $request->data;

        $check = Mfunction::where('id', $data['id'])->first();
        if ($check) {
            DB::beginTransaction();

            $company = Mfunction::findOrFail($data['id']);
            $company->update($data);

            DB::commit();
            $response = [
              'status' => Response::HTTP_OK,
              'message' => 'successfully updated.',
              'data' => $data,
            ];
            return response()->json($response, Response::HTTP_OK);
        } else {


          $response = [
            'status' => Response::HTTP_FOUND,
            'message' => 'The given data was invalid.',
            'data' => null,
          ];
          return response()->json($response, Response::HTTP_FOUND);
        }
      } catch (\Exception $e) {
        DB::rollback();
        $response = [
          'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
          'message' => 'The given data was invalid.' . $e->getMessage(),
          'data' => null,
        ];
        return response()->json($response, Response::HTTP_INTERNAL_SERVER_ERROR);
      }
  }

  public function getall(Request $request)
  {
    $query = Mfunction::query();

    // Apply filters
    if ($request->has('filtersparams')) {
        $filters = $request->input('filtersparams');

        foreach ($filters as $column => $filter) {
        $operator = $filter['operator'];
        $value = isset($filter['value']) ? $filter['value'] : null;

        if ($value !== null && $value !== '') {
            if ($operator === 'between') {
            $value = explode(',', $value);
            $query->whereBetween($column, $value);
            } elseif ($operator === 'like') {
            $query->where($column, $operator, '%' . $value . '%');
            } else {
            $query->where($column, $operator, $value);
            }
        }
        }
    }
    $result = $query->get();
    $total = $query->count();

    $response = [
        'status' => Response::HTTP_OK,
        'message' => 'successfully added.',
        'data' => $result,
        'total' => $total,
      ];
      return response()->json($response, Response::HTTP_OK);

    return response()->json([
        'data' => $result

    ]);
  }

  public function getone(Request $request)
  {
    $data = $request->params;

    $records = Mfunction::where('id', $data['id'])->first();
    $response = [
      'status' => Response::HTTP_OK,
      'message' => '0',
      'data' => $records,
    ];
    return response()->json($response, Response::HTTP_OK);
  }


}
