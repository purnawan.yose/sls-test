<?php

namespace App\Http\Controllers;

use App\Models\Mroleparent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    //
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $remember = $request->has('rememberMe');

        if (Auth::attempt($request->only('email', 'password'), $remember)) {
            $user = $request->user(); // Get the authenticated user
            $token = $request->user()->createToken('token-name')->plainTextToken;

            $role = Mroleparent::where('id', $user->role)->first();
            $user['role'] = $role->rolename;
            $user['username'] = 'admin';
            $response = [
                'status' => Response::HTTP_OK,
                'message' => 'successfully login.',
                'data' => [
                    'accessToken' => $token,
                    'tokenType' => 'Be$uarer',
                    'userData' => $user, // Additional user data
                ],
            ];
             return response()->json($response, Response::HTTP_OK);
        }


        $response = [
            'status' => Response::HTTP_UNAUTHORIZED,
            'message' => 'The provided credentials are incorrect.',
            'data' => null,
        ];
        return response()->json($response, Response::HTTP_UNAUTHORIZED);
    }

    public function me(Request $request){

        $user = $request->user(); // Get the authenticated user
        $role = Mroleparent::where('id', $user->role)->first();
        $user['role'] = $role->rolename;
        $user['username'] = 'admin';

        return response()->json(['user' => $user]);
    }
}
