<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mmenu extends Model
{
    use HasFactory;
    protected $table = 'mmenus';
    public $timestamps = true;

    use HasUuids;

    protected $fillable = [
        'id' ,
        'menuname' ,
        'menuroute',
        'remark' ,
        'parentid' ,
        'menuicon',
        'created_at' ,
        'updated_at' ,
        'created_by' ,
        'updated_by' ,
        'disabled' 
    ];
}
