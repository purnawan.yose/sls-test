<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mfunction extends Model
{
    use HasFactory;
    protected $table = 'mfunctions';
    public $timestamps = true;

    use HasUuids;

    protected $fillable = [
        'id',
        'functionname',
        'created_by',
        'updated_by',
    ];
}
