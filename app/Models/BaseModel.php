<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
  /**
   * Boot the base model.
   */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $user = User::find(Auth::id());
      $model->created_by = $user->name;
    });

    static::updating(function ($model) {
      $user = User::find(Auth::id());
      $model->created_by = $user->name;
    });
  }
}
