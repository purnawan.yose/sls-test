<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MfunctionController;
use App\Http\Controllers\MmenuController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//auth login
Route::post('/auth/login', [AuthController::class, 'login']);


Route::middleware(['auth:sanctum'])->group(function () {
    // Your authenticated routes go here
    Route::get('/auth/me', [AuthController::class, 'me']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/mfunction/create', [MfunctionController::class, 'create']);
    Route::post('/mfunction/update', [MfunctionController::class, 'update']);
    Route::post('/mfunction/getall', [MfunctionController::class, 'getall']);
    Route::post('/mfunction/getone', [MfunctionController::class, 'getone']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/mmenu/create', [MmenuController::class, 'create']);
    Route::post('/mmenu/update', [MmenuController::class, 'update']);
    Route::post('/mmenu/getall', [MmenuController::class, 'getall']);
    Route::post('/mmenu/getone', [MmenuController::class, 'getone']);
});